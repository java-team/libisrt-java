package de.intarsys.tools.json;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import de.intarsys.tools.converter.ConversionException;

public class Json {

	static public JsonArray createArray() {
		JsonArray result = new JsonArray();
		return result;
	}

	static public JsonArray createArray(Collection collection) {
		JsonArray result = new JsonArray();
		if (collection != null) {
			Iterator iter = collection.iterator();
			while (iter.hasNext()) {
				result.basicAdd(Json.wrap(iter.next()));
			}
		}
		return result;
	}

	static public JsonObject createObject() {
		JsonObject result = new JsonObject();
		return result;
	}

	static public JsonObject createObject(Map<String, Object> map) {
		JsonObject result = new JsonObject();
		if (map != null) {
			Iterator<Map.Entry<String, Object>> i = map.entrySet().iterator();
			while (i.hasNext()) {
				Map.Entry<String, Object> e = i.next();
				Object value = e.getValue();
				if (value != null) {
					result.basicPut(e.getKey(), wrap(value));
				}
			}
		}
		return result;
	}

	public static String doubleToString(double d) {
		if (Double.isInfinite(d) || Double.isNaN(d)) {
			return "null";
		}
		String string = Double.toString(d);
		if (string.indexOf('.') > 0 && string.indexOf('e') < 0
				&& string.indexOf('E') < 0) {
			while (string.endsWith("0")) {
				string = string.substring(0, string.length() - 1);
			}
			if (string.endsWith(".")) {
				string = string.substring(0, string.length() - 1);
			}
		}
		return string;
	}

	public static String numberToString(Number number) {
		// Shave off trailing zeros and decimal point, if possible.
		String string = number.toString();
		if (string.indexOf('.') > 0 && string.indexOf('e') < 0
				&& string.indexOf('E') < 0) {
			while (string.endsWith("0")) {
				string = string.substring(0, string.length() - 1);
			}
			if (string.endsWith(".")) {
				string = string.substring(0, string.length() - 1);
			}
		}
		return string;
	}

	public static String quote(String string) {
		StringWriter w = new StringWriter();
		try {
			JsonWriter.quote(string, w);
			return w.toString();
		} catch (IOException ignored) {
			return "";
		}
	}

	static public boolean toBoolean(Object object) throws ConversionException {
		if (object == null) {
		} else if (object.equals(Boolean.FALSE)
				|| (object instanceof String && ((String) object)
						.equalsIgnoreCase("false"))) {
			return false;
		} else if (object.equals(Boolean.TRUE)
				|| (object instanceof String && ((String) object)
						.equalsIgnoreCase("true"))) {
			return true;
		}
		throw new ConversionException("not a boolean.");
	}

	static public double toDouble(Object object) throws ConversionException {
		try {
			if (object == null) {
			} else if (object instanceof Number) {
				return ((Number) object).doubleValue();
			} else if (object instanceof String) {
				return Double.parseDouble((String) object);
			}
		} catch (Exception e) {
			//
		}
		throw new ConversionException("not a double");
	}

	static public int toInt(Object object) throws ConversionException {
		try {
			if (object == null) {
			} else if (object instanceof Number) {
				return ((Number) object).intValue();
			} else if (object instanceof String) {
				return Integer.parseInt((String) object);
			}
		} catch (Exception e) {
			//
		}
		throw new ConversionException("not an int");
	}

	static public long toLong(Object object) throws ConversionException {
		try {
			if (object == null) {
			} else if (object instanceof Number) {
				return ((Number) object).longValue();
			} else if (object instanceof String) {
				return Long.parseLong((String) object);
			}
		} catch (Exception e) {
			//
		}
		throw new ConversionException("not an int");
	}

	static public String toString(Object object) throws ConversionException {
		if (object == null) {
			return null;
		} else if (object instanceof String) {
			return (String) object;
		} else {
			return object.toString();
		}
	}

	public static String valueToString(Object value) throws IOException {
		if (value == null) {
			return "null";
		}
		if (value instanceof Number) {
			return numberToString((Number) value);
		}
		if (value instanceof Boolean || value instanceof JsonObject
				|| value instanceof JsonArray) {
			return value.toString();
		}
		return quote(value.toString());
	}

	public static Object wrap(Object object) {
		try {
			if (object == null) {
				return null;
			}
			if (object instanceof JsonObject || object instanceof JsonArray
					|| object instanceof Number || object instanceof Boolean
					|| object instanceof String) {
				return object;
			}
			if (object instanceof Collection) {
				return createArray((Collection) object);
			}
			if (object instanceof Map) {
				return createObject((Map) object);
			}
			return object.toString();
		} catch (Exception exception) {
			return null;
		}
	}

}
