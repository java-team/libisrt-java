package de.intarsys.tools.reflect;

/**
 * Simple adapter for {@link IRelationHandler}
 * 
 */
public class RelationHandlerAdapter implements IRelationHandler {

	@Override
	public Object[] get(Object owner) {
		return new Object[0];
	}

	@Override
	public Object getOwner(Object element) {
		return null;
	}

	@Override
	public Object insert(Object owner, Object value) {
		return value;
	}

	@Override
	public Object remove(Object owner, Object value) {
		return value;
	}

	@Override
	public int size(Object owner) {
		return 0;
	}

	@Override
	public Object update(Object owner, Object value, Object newValue) {
		return newValue;
	}
}
