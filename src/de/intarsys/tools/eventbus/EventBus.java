package de.intarsys.tools.eventbus;

/**
 * A VM singleton for accessing a global {@link IEventBus}.
 * 
 */
public class EventBus {

	private static IEventBus ACTIVE = new StandardEventBus();

	static public IEventBus get() {
		return ACTIVE;
	}

	static public void set(IEventBus bus) {
		ACTIVE = bus;
	}
}
