package de.intarsys.tools.eventbus;

import javax.annotation.PostConstruct;

import de.intarsys.tools.component.ConfigurationException;
import de.intarsys.tools.event.Event;
import de.intarsys.tools.event.EventType;
import de.intarsys.tools.event.INotificationListener;
import de.intarsys.tools.functor.Args;
import de.intarsys.tools.functor.FunctorCall;
import de.intarsys.tools.functor.FunctorInvocationException;
import de.intarsys.tools.functor.IArgs;
import de.intarsys.tools.functor.IFunctor;
import de.intarsys.tools.functor.IFunctorCall;
import de.intarsys.tools.infoset.ElementTools;
import de.intarsys.tools.infoset.IElement;
import de.intarsys.tools.infoset.IElementConfigurable;

/**
 * This object describes a subscription to the {@link IEventBus}.
 * 
 */
public class FunctorSubscription implements IElementConfigurable {

	private final INotificationListener listener = new INotificationListener() {
		@Override
		public void handleEvent(Event event) {
			onNotification(event);
		}
	};

	private EventType type;

	private EventSourcePredicate predicate;

	private IFunctor functor;

	@Override
	public void configure(IElement element) throws ConfigurationException {
		try {
			Class eventClazz = ElementTools.createClass(element, "event",
					Event.class, null);
			if (eventClazz == null) {
				type = EventType.ALWAYS;
			} else {
				type = EventType.lookupEventType(eventClazz.getName());
			}
			Class sourceClazz = ElementTools.createClass(element, "source",
					Object.class, null);
			if (sourceClazz == null) {
				predicate = new EventSourceAll();
			} else {
				predicate = new EventSourceClass(sourceClazz);
			}
			functor = ElementTools
					.createFunctor(this, element, "functor", null);
		} catch (Exception e) {
			throw new ConfigurationException(e);
		}
	}

	protected void onNotification(Event event) {
		try {
			IArgs args = Args.create();
			IFunctorCall call = new FunctorCall(this, args);
			functor.perform(call);
		} catch (FunctorInvocationException e) {
			//
			e.printStackTrace();
		}
	}

	@PostConstruct
	public void postConstruct() {
		EventBus.get().subscribe(predicate, type, listener);
	}
}
