package de.intarsys.tools.eventbus;

/**
 * A predicate for accepting source objects of a certain class.
 * 
 */
public class EventSourceClass extends EventSourcePredicate {

	private Class clazz;

	public EventSourceClass(Class clazz) {
		super();
		this.clazz = clazz;
	}

	@Override
	public boolean accepts(Object source) {
		return clazz.isInstance(source);
	}

	public Class getClazz() {
		return clazz;
	}

	public void setClazz(Class clazz) {
		this.clazz = clazz;
	}
}
