package de.intarsys.tools.url;

import de.intarsys.tools.string.StringTools;

public class URLTools {

	/**
	 * Ensure path has a leading path separator char.
	 * 
	 * @param path
	 * @return
	 */
	public static String withLeadingSeparator(String path) {
		if (StringTools.isEmpty(path)) {
			return "/";
		}
		if (path.startsWith("/")) {
			return path;
		}
		return "/" + path;
	}

	/**
	 * Ensure path has no leading path separator char.
	 * 
	 * @param path
	 * @return
	 */
	public static String withoutLeadingSeparator(String path) {
		if (StringTools.isEmpty(path)) {
			return "";
		}
		while (path.startsWith("/")) {
			path = path.substring(1);
		}
		return path;
	}

	/**
	 * Ensure path has no trailing path separator char.
	 * 
	 * @param path
	 * @return
	 */
	public static String withoutTrailingSeparator(String path) {
		if (StringTools.isEmpty(path)) {
			return "";
		}
		while (path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}
		return path;
	}

	/**
	 * Ensure path has a trailing path separator char.
	 * 
	 * @param path
	 * @return
	 */
	public static String withTrailingSeparator(String path) {
		if (StringTools.isEmpty(path)) {
			return "";
		}
		if (path.endsWith("/")) {
			return path;
		}
		return path + "/";
	}

	private URLTools() {
	}

}
