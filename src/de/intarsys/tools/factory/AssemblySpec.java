package de.intarsys.tools.factory;

import de.intarsys.tools.functor.ArgTools;
import de.intarsys.tools.functor.Args;
import de.intarsys.tools.functor.IArgs;
import de.intarsys.tools.reflect.ObjectCreationException;
import de.intarsys.tools.string.StringTools;

/**
 * Iconification of a factory call. A "factory call" consists of
 * <ul>
 * <li>a factory</li>
 * <li>arguments</li>
 * <ul>
 * 
 * There are numerous ways to create a valid {@link AssemblySpec}.
 * 
 * <pre>
 * canonical default role 
 * {
 * 		object: {
 * 			factory: <polymorphic factory object>,
 * 			args: <iargs structure>
 * 		}
 * }
 * 
 * <pre>
 * canonical role based 
 * {
 * 		<role>: {
 * 			factory: <polymorphic factory object>,
 * 			args: <iargs structure>
 * 		}
 * }
 * 
 * <pre>
 * deprecated embedded form
 * {
 * 		processor/class/factory: <polymorphic processor factory>,
 * 		... <more args> ...
 * }
 * 
 * <pre>
 * deprecated separated form
 * {
 * 		processor: <polymorphic processor factory>,
 * 		args: <iargs structure>
 * }
 * 
 * <pre>
 * deprecated role based, separated 
 * {
 * 		role: <polymorphic processor factory>,
 * 		roleArgs: <iargs structure>
 * }
 * 
 */
public class AssemblySpec {

	public static AssemblySpec create(Object factory, IArgs args)
			throws ObjectCreationException {
		AssemblySpec result = new AssemblySpec();
		result.setFactory(FactoryTools.createFactory(factory, null));
		result.setArgs(args);
		return result;
	}

	static public AssemblySpec createFromArgs(IArgs args)
			throws ObjectCreationException {
		return createFromArgs(args, "factory", "args");
	}

	static public AssemblySpec createFromArgs(IArgs args, String role)
			throws ObjectCreationException {
		return createFromArgs(args, role, ArgTools.prefix(role, "args"));
	}

	public static AssemblySpec createFromArgs(IArgs args, String factoryName,
			String argsName) throws ObjectCreationException {
		AssemblySpec result = new AssemblySpec();
		Object factory = ArgTools.getPath(args, factoryName);
		result.setFactory(FactoryTools.createFactory(factory, null));
		result.setArgs(ArgTools.getArgs(args, argsName, Args.create()));
		return result;
	}

	static public AssemblySpec createFromArgsInline(IArgs args)
			throws ObjectCreationException {
		AssemblySpec result = new AssemblySpec();
		Object factory = ArgTools.getPath(args, "processor");
		result.setFactory(FactoryTools.createFactory(factory, null));
		result.setArgs(args);
		return result;
	}

	private IFactory factory;

	private IArgs args;

	protected AssemblySpec() {
	}

	public Object createInstance() throws ObjectCreationException {
		if (getFactory() == null) {
			throw new ObjectCreationException("No factory");
		}
		return getFactory().createInstance(getArgs());
	}

	public IArgs getArgs() {
		return args;
	}

	public IFactory getFactory() {
		return factory;
	}

	/**
	 * Merge the {@link AssemblySpec} into args
	 * 
	 * @param args
	 * @param role
	 */
	public void serializeTo(IArgs args, String role) {
		if (StringTools.isEmpty(role)) {
			role = "object";
		}
		args.put(role, toArgs());
	}

	public void setArgs(IArgs args) {
		this.args = args;
	}

	public void setFactory(IFactory factory) {
		this.factory = factory;
	}

	/**
	 * Create the standard arguments form of the spec.
	 * 
	 */
	public IArgs toArgs() {
		return Args.createNamed( //
				"factory", getFactory(), //
				"args", getArgs());
	}

}
