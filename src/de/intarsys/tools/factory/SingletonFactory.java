package de.intarsys.tools.factory;

import de.intarsys.tools.functor.IArgs;
import de.intarsys.tools.reflect.ObjectCreationException;

public class SingletonFactory<T> implements IFactory<T> {

	final private T object;

	public SingletonFactory(T object) {
		this.object = object;
	}

	@Override
	public T createInstance(IArgs args) throws ObjectCreationException {
		return object;
	}

	@Override
	public String getId() {
		return null;
	}

	public T getObject() {
		return object;
	}

	@Override
	public Class<T> getResultType() {
		return (Class<T>) object.getClass();
	}

}
