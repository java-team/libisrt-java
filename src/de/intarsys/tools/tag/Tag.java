package de.intarsys.tools.tag;

import de.intarsys.tools.string.StringTools;

/**
 * A simple key/value pair that can be attached to POJO's.
 * <p>
 * Tags can be created using strings of the format <br>
 * <code>
 * key[=value]?[;key[=value]?]*
 * </code> or <code>
 * key[:value]?[;key[:value]?]*
 * </code>
 * 
 */
public class Tag {

	final private String key;

	final private String value;

	public Tag(String key) {
		super();
		this.key = key;
		this.value = "";
	}

	public Tag(String key, String value) {
		super();
		this.key = key;
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Tag) {
			return equalsFromTag((Tag) obj);
		}
		return false;
	}

	protected boolean equalsFromTag(Tag obj) {
		return obj.value.equals(this.value) && obj.key.equals(this.key);
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		return key.hashCode() + value.hashCode();
	}

	@Override
	public String toString() {
		if (StringTools.isEmpty(value)) {
			return key;
		} else {
			return key + "=" + value;
		}
	}

}
