package de.intarsys.tools.component;

import de.intarsys.tools.attribute.AttributeMap;

/**
 * A simple standard implementation for {@link IActivityContext}.
 * 
 */
public class StandardActivityContext implements IActivityContext {

	final private AttributeMap attributes = new AttributeMap();

	private int ref = 0;

	final private Object lock = new Object();

	@Override
	public Object acquire() {
		synchronized (lock) {
			ref++;
		}
		return null;
	}

	protected void basicRelease() {
		Object[] keys = attributes.getKeys();
		for (Object key : keys) {
			Object value = attributes.get(key);
			if (value instanceof IDisposable) {
				((IDisposable) value).dispose();
			}
		}
		attributes.clear();
	}

	@Override
	public Object getAttribute(Object key) {
		return attributes.getAttribute(key);
	}

	@Override
	public int getReferenceCount() {
		synchronized (lock) {
			return ref;
		}
	}

	@Override
	@Deprecated
	public void release() {
		release(null);
	}

	@Override
	public void release(Object handle) {
		int tempRef;
		synchronized (lock) {
			tempRef = --ref;
		}
		if (tempRef == 0) {
			basicRelease();
		}
	}

	@Override
	public Object removeAttribute(Object key) {
		return attributes.removeAttribute(key);
	}

	@Override
	public Object setAttribute(Object key, Object value) {
		synchronized (lock) {
			// stop writing if not active, prevent resource leaks as this will
			// get never released. As it is not acquired, nobody will be
			// interested in the information anyway
			if (ref <= 0) {
				return null;
			}
			return attributes.setAttribute(key, value);
		}
	}
}
