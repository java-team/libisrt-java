package de.intarsys.tools.component;

/**
 * A VM singleton to access an {@link IActivityContext}.
 * 
 */
public class ActivityContext {

	private static ThreadLocal<IActivityContext> context = new ThreadLocal<>();

	/**
	 * The current activity context. This will never return null.
	 * 
	 * @return
	 */
	static public IActivityContext get() {
		IActivityContext temp = context.get();
		if (temp == null) {
			temp = new StandardActivityContext();
			context.set(temp);
		}
		return temp;
	}

}
