package de.intarsys.tools.expression;

import de.intarsys.tools.component.ActivityLocal;
import de.intarsys.tools.component.IActivityContext;
import de.intarsys.tools.functor.IArgs;

/**
 * Forward evaluation to a {@link ScopedResolver} attached to an
 * {@link IActivityContext}.
 * 
 * @see ThreadContextAwareResolver
 * 
 */
public class ActivityContextAwareResolver implements IStringEvaluator {

	private static ActivityLocal<ScopedResolver> resolver = new ActivityLocal<>();

	synchronized public static void add(IStringEvaluator pResolver) {
		ScopedResolver tempResolver = resolver.get();
		if (tempResolver == null) {
			tempResolver = new ScopedResolver();
			resolver.set(tempResolver);
		}
		tempResolver.addResolver(pResolver);
	}

	synchronized public static IStringEvaluator add(String key,
			IStringEvaluator pResolver) {
		ScopedResolver tempResolver = resolver.get();
		if (tempResolver == null) {
			tempResolver = new ScopedResolver();
			resolver.set(tempResolver);
		}
		MapResolver newResolver = MapResolver.create(key, pResolver);
		tempResolver.addResolver(newResolver);
		return newResolver;
	}

	synchronized public static void remove(IStringEvaluator pResolver) {
		ScopedResolver tempResolver = resolver.get();
		if (resolver == null) {
			return;
		}
		tempResolver.removeResolver(pResolver);
	}

	@Override
	public Object evaluate(String expression, IArgs args)
			throws EvaluationException {
		ScopedResolver tempResolver = resolver.get();
		if (tempResolver == null) {
			throw new EvaluationException("can't evaluate '" + expression + "'");
		}
		return tempResolver.evaluate(expression, args);
	}

}
