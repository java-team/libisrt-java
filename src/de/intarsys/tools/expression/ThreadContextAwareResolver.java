package de.intarsys.tools.expression;

import de.intarsys.tools.functor.IArgs;

public class ThreadContextAwareResolver implements IStringEvaluator {

	private static ThreadLocal<ScopedResolver> resolver = new ThreadLocal<>();

	synchronized public static void add(IStringEvaluator pResolver) {
		ScopedResolver tempResolver = resolver.get();
		if (tempResolver == null) {
			tempResolver = new ScopedResolver();
			resolver.set(tempResolver);
		}
		tempResolver.addResolver(pResolver);
	}

	synchronized public static void remove(IStringEvaluator pResolver) {
		ScopedResolver tempResolver = resolver.get();
		if (resolver == null) {
			return;
		}
		tempResolver.removeResolver(pResolver);
	}

	@Override
	public Object evaluate(String expression, IArgs args)
			throws EvaluationException {
		ScopedResolver tempResolver = resolver.get();
		if (tempResolver == null) {
			throw new EvaluationException("can't evaluate '" + expression + "'");
		}
		return tempResolver.evaluate(expression, args);
	}

}
