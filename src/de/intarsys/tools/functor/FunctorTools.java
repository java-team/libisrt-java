package de.intarsys.tools.functor;

import de.intarsys.tools.factory.FactoryTools;
import de.intarsys.tools.factory.IFactory;
import de.intarsys.tools.reflect.ObjectCreationException;

public class FunctorTools {

	/**
	 * Create an {@link IFunctor} from an object.
	 * 
	 * @param object
	 * @param defaultValue
	 *            The default value to be used
	 * @return An {@link IFunctor}
	 * 
	 * @throws ObjectCreationException
	 */
	public static IFunctor createFunctor(Object value, Object defaultValue)
			throws ObjectCreationException {
		if (value == null) {
			value = defaultValue;
		}
		if (value instanceof IFunctor) {
			return (IFunctor) value;
		}
		if (value instanceof String) {
			value = FactoryTools.lookupFactory((String) value, null);
		}
		if (value instanceof IFactory) {
			return (IFunctor) ((IFactory) value).createInstance(Args.create());
		}
		throw new ObjectCreationException("can not create processor factory");
	}

	private FunctorTools() {
	}

}
