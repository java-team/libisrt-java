package de.intarsys.tools.net;

import javax.net.ssl.HostnameVerifier;

import de.intarsys.tools.component.ConfigurationException;
import de.intarsys.tools.infoset.IElement;
import de.intarsys.tools.infoset.IElementConfigurable;
import de.intarsys.tools.infoset.IElementSerializable;

/**
 * Common superclass for implementing {@link HostnameVerifier}.
 * 
 */
abstract public class CommonHostnameVerifier implements HostnameVerifier,
		IElementConfigurable, IElementSerializable {

	@Override
	public void configure(IElement element) throws ConfigurationException {

	}

	@Override
	public void serialize(IElement element)
			throws de.intarsys.tools.infoset.ElementSerializationException {
		element.setAttributeValue("class", getClass().getName());
	};

}
