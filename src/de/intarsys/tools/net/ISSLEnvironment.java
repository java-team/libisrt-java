package de.intarsys.tools.net;

import java.security.GeneralSecurityException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;

/**
 * Provide a SSL environment abstraction for creating preconfigured
 * {@link SSLContext} instances.
 * 
 */
public interface ISSLEnvironment {

	/**
	 * The default {@link SSLContext} to be used in this environment.
	 * 
	 * @return
	 * @throws GeneralSecurityException
	 */
	public SSLContext createSslContext() throws GeneralSecurityException;

	public SSLContext createSslContext(KeyManager[] keyManagers)
			throws GeneralSecurityException;
}
