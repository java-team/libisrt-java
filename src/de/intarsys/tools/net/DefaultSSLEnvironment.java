package de.intarsys.tools.net;

import java.security.GeneralSecurityException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;

/**
 * The default {@link ISSLEnvironment} falls back to the default java
 * implementation.
 * 
 */
public class DefaultSSLEnvironment implements ISSLEnvironment {

	@Override
	public SSLContext createSslContext() throws GeneralSecurityException {
		return SSLContext.getDefault();
	}

	@Override
	public SSLContext createSslContext(KeyManager[] keyManagers)
			throws GeneralSecurityException {
		throw new UnsupportedOperationException();
	}
}
