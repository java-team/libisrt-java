package de.intarsys.tools.net;

import javax.net.ssl.SSLSession;

/**
 * Accept local hostnames in addition to the {@link StrictHostnameVerifier}.
 * 
 * TODO this is not yet implemented....
 * 
 */
public class AcceptLocalHostnameVerifier extends CommonHostnameVerifier {

	@Override
	public boolean verify(String hostname, SSLSession session) {
		return true;
	}

}
