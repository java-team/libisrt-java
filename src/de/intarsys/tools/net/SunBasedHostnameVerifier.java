package de.intarsys.tools.net;

import java.security.Principal;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.security.auth.kerberos.KerberosPrincipal;

import sun.security.util.HostnameChecker;

/**
 * Hostname verification based on the proprietary SUN implementation.
 * 
 * You should not use this one directly, but via the
 * {@link StrictHostnameVerifier} only. This is a class of its own to defer and
 * catch {@link NoClassDefFoundError}.
 * 
 */
class SunBasedHostnameVerifier implements HostnameVerifier {

	@Override
	public boolean verify(String hostname, SSLSession session) {
		HostnameChecker checker = HostnameChecker
				.getInstance(HostnameChecker.TYPE_TLS);
		try {
			Certificate[] peerCertificates = session.getPeerCertificates();
			if (peerCertificates.length > 0
					&& peerCertificates[0] instanceof X509Certificate) {
				X509Certificate peerCertificate = (X509Certificate) peerCertificates[0];
				try {
					checker.match(hostname, peerCertificate);
					return true;
				} catch (CertificateException ex) {
					// Certificate does not match hostname
				}
			} else {
				// Peer does not have any certificates or they aren't X.509
			}
		} catch (SSLPeerUnverifiedException ex) {
			// Not using certificates for peers, try verifying the principal
			try {
				Principal peerPrincipal = session.getPeerPrincipal();
				if (peerPrincipal instanceof KerberosPrincipal) {
					return HostnameChecker.match(hostname, peerPrincipal);
				} else {
					// Can't verify principal, not Kerberos
				}
			} catch (SSLPeerUnverifiedException ex2) {
				// Can't verify principal, no principal
			}
		}
		return false;
	}
}
