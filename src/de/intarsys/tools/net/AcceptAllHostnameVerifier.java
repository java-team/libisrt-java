package de.intarsys.tools.net;

import javax.net.ssl.SSLSession;

/**
 * Accept all hostnames.
 * 
 * Do use this with care - better, do not use it all.
 * 
 */
public class AcceptAllHostnameVerifier extends CommonHostnameVerifier {

	@Override
	public boolean verify(String hostname, SSLSession session) {
		return true;
	}

}
