package de.intarsys.tools.net;

/**
 * A VM singleton for the {@link ISSLEnvironment}.
 * 
 */
public class SSLEnvironment {

	private static ISSLEnvironment Active;

	synchronized public static ISSLEnvironment get() {
		if (Active == null) {
			Active = new DefaultSSLEnvironment();
		}
		return Active;
	}

	synchronized public static void set(ISSLEnvironment active) {
		Active = active;
	}

}
