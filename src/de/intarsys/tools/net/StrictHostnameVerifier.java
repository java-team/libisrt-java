package de.intarsys.tools.net;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import de.intarsys.tools.logging.LogTools;
import de.intarsys.tools.reflect.ObjectTools;

/**
 * Use the default hostname verifier.
 * 
 * This is either the Apache HTTP client implementation or the sun builtin
 * implementation. Otherwise the access is denied.
 * 
 */
public class StrictHostnameVerifier extends CommonHostnameVerifier {

	private static final Logger Log = LogTools
			.getLogger(StrictHostnameVerifier.class);

	private static HostnameVerifier implementor;

	static {
		// try some dirty reflection.
		// either we are on a Apache HTTPClient aware system...
		try {
			implementor = ObjectTools.createObject(
					"org.apache.http.conn.ssl.StrictHostnameVerifier",
					HostnameVerifier.class, null);
		} catch (Throwable e1) {
			Log.log(Level.WARNING,
					"Apache HTTP client hostname verifier not found");
			// or we can use SUNs implementation
			try {
				implementor = ObjectTools.createObject(
						"de.intarsys.tools.net.SunBasedHostnameVerifier",
						HostnameVerifier.class, null);
			} catch (Throwable e2) {
				Log.log(Level.WARNING, "SUN hostname verifier not found");
				// or we fail...
				Log.log(Level.WARNING,
						"We will deny all hostnames on SSL connections");
				implementor = new DenyAllHostnameVerifier();
			}
		}
	}

	@Override
	public boolean verify(String hostname, SSLSession session) {
		return implementor.verify(hostname, session);
	}

}
