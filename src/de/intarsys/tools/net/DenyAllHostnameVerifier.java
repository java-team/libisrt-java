package de.intarsys.tools.net;

import javax.net.ssl.SSLSession;

/**
 * Deny all hostnames.
 * 
 */
public class DenyAllHostnameVerifier extends CommonHostnameVerifier {

	@Override
	public boolean verify(String hostname, SSLSession session) {
		return false;
	}

}
