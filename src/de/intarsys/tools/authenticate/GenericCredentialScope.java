package de.intarsys.tools.authenticate;

public class GenericCredentialScope implements ICredentialScope {

	private final ICredentialSpec credentialSpec;

	private String prompt;

	public GenericCredentialScope(ICredentialSpec credentialSpec) {
		super();
		this.credentialSpec = credentialSpec;
	}

	@Override
	public ICredentialSpec getCredentialSpec() {
		return credentialSpec;
	}

	@Override
	public String getPrompt() {
		return prompt;
	}

	@Override
	public int match(ICredentialScope scope) {
		if (scope == this) {
			return 1;
		}
		return -1;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

}
